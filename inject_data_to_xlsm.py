import numpy as np
import xlwings as xw

## Create an array of 5 rows and 3 columns with random data
arrays = np.random.rand(5,3)*100 

excel = xw.Book("project/project.xlsm") ## Open Excel file
excel_sheet = excel.sheets[0] ## Select a sheet
excel_sheet.range("A1").value = arrays ## Inject the array previously created in the sheet

excel.save() ## Save the workbook
excel.close() ## Close the workbook