## Xlwings - basics

### Install

```
pip install xlwings
```

or 
```
conda install -c conda-forge xlwings
```

then 
```
xlwings addin install
```

### Create a project

First, we need to create a workspace. To do so  we use the command : 
```
xlwings quickstart {projectname}
```
There is also an other way to setup our environment : 
--> https://docs.xlwings.org/en/stable/quickstart.html#macros-call-python-from-excel

### Scripts explained
- **inject_data_to_xlsm.py** generate a random array and inject it to our Excel file ( the file's name need to be changed)
-  **project. py** contain our functions.

### Basic functions

 In the Notebook xlwings_basics.ipynb there is some basic function examples
 
 