import xlwings as xw

@xw.func
def hello(name):
    return f"Hello {name}!"

@xw.func
def test_mult(cels):
    
    multiplied_matrice = []
    for l in cels: 
        multiplied_list = [element * 2 for element in l]
        multiplied_matrice.append(multiplied_list) 
    return multiplied_matrice